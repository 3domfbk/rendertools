package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "rendertools",
	Short: "rendertools is a collection of tools to render 3D geometry in OpenGL",
	Long: `A smart solution to all of your rendering problems, features tools to render
geometry of all formats and kinds`,
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
