package cmd

import (
	"fmt"
	"log"
	"runtime"

	"bitbucket.org/3domfbk/commontools/graphics"
	"bitbucket.org/3domfbk/meshtools/mt"
	"bitbucket.org/3domfbk/pctools/pct"
	"bitbucket.org/3domfbk/rendertools/rt"

	"github.com/go-gl/glfw/v3.1/glfw"
	"github.com/spf13/cobra"
)

var displayWidth, displayHeight *int
var displayClearR, displayClearG, displayClearB *float32

// ortho2pcCmd represents the aggregate command
var displayCmd = &cobra.Command{
	Use:   "display obj1 obj2 ...",
	Short: "Displays a 3D object",
	Long:  `Displays the specified objects inside a GL window`,

	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			cmd.Usage()
			return
		}

		// Do it
		display(args)
	},
}

func init() {
	RootCmd.AddCommand(displayCmd)

	displayWidth = displayCmd.Flags().IntP("width", "W", 800, "Window width")
	displayHeight = displayCmd.Flags().IntP("height", "H", 600, "Window height")
	displayClearR = displayCmd.Flags().Float32P("clearr", "r", 0.25, "Clear color red component")
	displayClearG = displayCmd.Flags().Float32P("clearg", "g", 0.25, "Clear color green component")
	displayClearB = displayCmd.Flags().Float32P("clearb", "b", 0.25, "Clear color blue component")
}

// State state
type State struct {
	Viewer *rt.Viewer
	Fov    float64
	x, y   float32
}

var state State

func display(objects []string) {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()

	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.ContextVersionMajor, 2)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	window, err := glfw.CreateWindow(*displayWidth, *displayHeight, "Render Tools", nil, nil)
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	window.SetScrollCallback(scrollCallback)
	window.SetMouseButtonCallback(mouseButtonCallback)
	window.SetCursorPosCallback(cursorPositionCallback)
	window.SetSizeCallback(windowSizeCallback)

	state.Viewer = rt.NewViewer()
	state.Fov = 45
	err = state.Viewer.Init(*displayWidth, *displayHeight)

	if err != nil {
		fmt.Printf("Could not initialize GL viewer: %s\n", err)
		return
	}

	state.Viewer.ClearColor = graphics.Color{R: *displayClearR, G: *displayClearG, B: *displayClearB, A: 1.0}

	// Read all objects and add them to the viewer
	for _, obj := range objects {
		mesh, err := mt.ReadMesh(obj)
		if err != nil {
			// Maybe you are a cloud?
			pc, err := pct.ReadPc(obj)
			if err != nil {
				fmt.Printf("Unable to load object: %s\n", err)
			} else {
				state.Viewer.AddObject(pc, false)
			}
		} else {
			state.Viewer.AddObject(mesh, false)
		}
	}

	for !window.ShouldClose() {
		state.Viewer.Render()
		window.SwapBuffers()
		glfw.PollEvents()
	}
}

func scrollCallback(w *glfw.Window, xoff float64, yoff float64) {

	state.Viewer.OnFOVChanged(xoff, yoff)
}

func mouseButtonCallback(w *glfw.Window, b glfw.MouseButton, action glfw.Action, mods glfw.ModifierKey) {

	switch action {
	case glfw.Press:
		state.Viewer.OnMousePressed(int(b), float32(state.x), float32(state.y))
		break

	case glfw.Release:
		state.Viewer.OnMouseReleased(int(b), float32(state.x), float32(state.y))
		break
	}
}

func cursorPositionCallback(w *glfw.Window, x, y float64) {
	state.x = float32(x)
	state.y = float32(y)
	state.Viewer.OnMouseMoved(state.x, state.y)
}

func windowSizeCallback(w *glfw.Window, x, y int) {
	state.Viewer.Resize(x, y)
}
