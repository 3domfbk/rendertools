package rt

import (
	"math"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/commontools/graphics"
	"github.com/go-gl/gl/v4.1-core/gl"
)

// Circle is a camera view frustum
type Circle struct {
	Color  graphics.Color
	Text   *Text
	Center geom.Vec3d
	Radius float64
}

// NewCircle creates a new frustum
func NewCircle(x, y, z, radius float64, text *Text, r, g, b float32) *Circle {
	text.Position = geom.Vec3d{X: x, Y: y, Z: z + radius/2}
	return &Circle{
		Center: geom.Vec3d{X: x, Y: y, Z: z},
		Radius: radius,
		Text:   text,
		Color:  graphics.Color{R: r, G: g, B: b, A: 1}}
}

// CircleView a frustum view
type CircleView struct {
	Circle   *Circle
	viewer   *Viewer
	textView *TextView
	vertices []geom.Vec3f
	colors   []float32
	elements []uint32
	allVbo   uint32
	dirty    bool
}

// NewCircleView creates a new text view
func NewCircleView(circle *Circle, viewer *Viewer) *CircleView {
	return &CircleView{Circle: circle, dirty: true, viewer: viewer, textView: NewTextView(circle.Text, viewer)}
}

// AABB override
func (view *CircleView) AABB() *geom.AABB {
	aabb := geom.NewAABB()
	aabb.ExtendTo(view.Circle.Center)
	aabb.ExtendTo(geom.Vec3d{
		X: view.Circle.Center.X - view.Circle.Radius,
		Y: view.Circle.Center.Y - view.Circle.Radius,
		Z: view.Circle.Center.Z - view.Circle.Radius})
	aabb.ExtendTo(geom.Vec3d{
		X: view.Circle.Center.X + view.Circle.Radius,
		Y: view.Circle.Center.Y + view.Circle.Radius,
		Z: view.Circle.Center.Z + view.Circle.Radius})
	return aabb
}

// Selection override
func (view *CircleView) Selection() interface{} {
	return nil
}

// ClearSelection override
func (view *CircleView) ClearSelection() {

}

// SetVisible override
func (view *CircleView) SetVisible(visible bool) {

}

// OnRender override
func (view *CircleView) OnRender() {

	if view.dirty {
		view.OnUpdate()
		view.dirty = false
	}

	gl.UseProgram(view.viewer.program)

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
	gl.EnableVertexAttribArray(view.viewer.vposLocation)
	gl.VertexAttribPointer(view.viewer.vposLocation, 3, gl.FLOAT, false, 0, gl.Ptr(view.vertices))
	gl.EnableVertexAttribArray(view.viewer.vcolLocation)
	gl.VertexAttribPointer(view.viewer.vcolLocation, 3, gl.FLOAT, false, 0, gl.Ptr(view.colors))

	gl.LineWidth(20.0 / view.viewer.distance)
	gl.Enable(gl.LINE_SMOOTH)
	gl.Enable(gl.DEPTH_TEST)
	gl.DrawElements(gl.LINE_LOOP, 360, gl.UNSIGNED_INT, gl.Ptr(view.elements))
	gl.Disable(gl.LINE_SMOOTH)
	gl.LineWidth(1.0)

	view.textView.OnRender()
}

// OnRelease override
func (view *CircleView) OnRelease() {

}

// OnUpdate override
func (view *CircleView) OnUpdate() {

	view.vertices = make([]geom.Vec3f, 0, 360)
	view.colors = make([]float32, 0, 360*3)
	view.elements = make([]uint32, 0, 360)

	for i := 0; i < 360; i++ {
		angle := float64(i) * 2.0 * math.Pi / 360.0

		view.vertices = append(view.vertices, geom.Vec3f{
			X: float32(view.Circle.Center.X + view.Circle.Radius*math.Cos(angle)),
			Y: float32(view.Circle.Center.Y + view.Circle.Radius*math.Sin(angle)),
			Z: float32(view.Circle.Center.Z)})

		view.colors = append(view.colors, view.Circle.Color.R)
		view.colors = append(view.colors, view.Circle.Color.G)
		view.colors = append(view.colors, view.Circle.Color.B)

		view.elements = append(view.elements, uint32(i))
	}

	view.textView.OnUpdate()

}

// OnRectSelect override
func (view *CircleView) OnRectSelect(vx, vy, vw, vh int) {

}
