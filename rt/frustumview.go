package rt

import (
	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/commontools/graphics"
	"bitbucket.org/3domfbk/meshtools/mt"
	"github.com/go-gl/gl/v4.1-core/gl"
)

// Frustum is a camera view frustum
type Frustum struct {
	Color graphics.Color
	Text  *Text

	P0, P1, P2, P3, P4 geom.Vec3d
}

//NewFrustum creates a new frustum
func NewFrustum(mesh *mt.Mesh, text *Text, r, g, b float32) *Frustum {
	text.Position = mesh.Positions()[4]
	text.Position.Z += text.Position.Dst(mesh.Positions()[0]) / 10
	return &Frustum{
		Text:  text,
		P0:    mesh.Positions()[4],
		P1:    mesh.Positions()[0],
		P2:    mesh.Positions()[1],
		P3:    mesh.Positions()[2],
		P4:    mesh.Positions()[3],
		Color: graphics.Color{R: r, G: g, B: b, A: 1}}
}

// FrustumView a frustum view
type FrustumView struct {
	Frustum  *Frustum
	viewer   *Viewer
	textView *TextView
	vertices []geom.Vec3f
	colors   []float32
	elements []uint32
	allVbo   uint32
	dirty    bool
}

// NewFrustumView creates a new text view
func NewFrustumView(frustum *Frustum, viewer *Viewer) *FrustumView {
	return &FrustumView{Frustum: frustum, dirty: true, viewer: viewer, textView: NewTextView(frustum.Text, viewer)}
}

// AABB override
func (view *FrustumView) AABB() *geom.AABB {
	aabb := geom.NewAABB()
	aabb.ExtendTo(view.Frustum.P0)
	aabb.ExtendTo(view.Frustum.P1)
	aabb.ExtendTo(view.Frustum.P2)
	aabb.ExtendTo(view.Frustum.P3)
	aabb.ExtendTo(view.Frustum.P4)
	return aabb
}

// Selection override
func (view *FrustumView) Selection() interface{} {
	return nil
}

// ClearSelection override
func (view *FrustumView) ClearSelection() {

}

// SetVisible override
func (view *FrustumView) SetVisible(visible bool) {

}

// OnRender override
func (view *FrustumView) OnRender() {

	if view.dirty {
		view.OnUpdate()
		view.dirty = false
	}

	gl.UseProgram(view.viewer.program)

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)

	gl.EnableVertexAttribArray(view.viewer.vposLocation)
	gl.VertexAttribPointer(view.viewer.vposLocation, 3, gl.FLOAT, false, 0, gl.Ptr(view.vertices))
	gl.EnableVertexAttribArray(view.viewer.vcolLocation)
	gl.VertexAttribPointer(view.viewer.vcolLocation, 3, gl.FLOAT, false, 0, gl.Ptr(view.colors))

	gl.Enable(gl.DEPTH_TEST)
	gl.DrawElements(gl.LINES, 8, gl.UNSIGNED_INT, gl.Ptr(view.elements))
	gl.DrawElements(gl.LINE_LOOP, 4, gl.UNSIGNED_INT, gl.Ptr(view.elements[8:]))

	view.textView.OnRender()
}

// OnRelease override
func (view *FrustumView) OnRelease() {

}

// OnUpdate override
func (view *FrustumView) OnUpdate() {

	view.vertices = []geom.Vec3f{
		geom.Vec3d2Vec3f(view.Frustum.P0),
		geom.Vec3d2Vec3f(view.Frustum.P1),
		geom.Vec3d2Vec3f(view.Frustum.P2),
		geom.Vec3d2Vec3f(view.Frustum.P3),
		geom.Vec3d2Vec3f(view.Frustum.P4)}

	view.colors = []float32{
		view.Frustum.Color.R, view.Frustum.Color.G, view.Frustum.Color.B,
		view.Frustum.Color.R, view.Frustum.Color.G, view.Frustum.Color.B,
		view.Frustum.Color.R, view.Frustum.Color.G, view.Frustum.Color.B,
		view.Frustum.Color.R, view.Frustum.Color.G, view.Frustum.Color.B,
		view.Frustum.Color.R, view.Frustum.Color.G, view.Frustum.Color.B}

	view.elements = []uint32{
		0, 1, 0, 2, 0, 3, 0, 4,
		1, 2, 3, 4}

	//gl.GenBuffers(1, &view.allVbo)
	//gl.BindBuffer(gl.ARRAY_BUFFER, view.allVbo)

	//gl.BufferData(gl.ARRAY_BUFFER, len(v.vertices)*int(unsafe.Sizeof(v.vertices[0])), gl.Ptr(v.vertices), gl.STATIC_DRAW)

	view.textView.OnUpdate()

}

// OnRectSelect override
func (view *FrustumView) OnRectSelect(vx, vy, vw, vh int) {

}
