package rt

import (
	"fmt"
	"strings"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/commontools/graphics"
	"bitbucket.org/3domfbk/meshtools/mt"
	"bitbucket.org/3domfbk/pctools/pct"
	"github.com/go-gl/gl/v4.1-core/gl"
)

// Standard Shaders
const (
	vShaderSource = `
		#version 120
		#ifdef GL_ES
			precision mediump float;
		#endif
		uniform mat4 MVP;
		attribute vec4 vCol;
		attribute vec3 vPos;
		varying vec4 color;
		void main()
		{
		    gl_PointSize = 1.0;
		    gl_Position = MVP * vec4(vPos, 1.0);
		    color = vCol;
		}` + "\x00"

	fShaderSource = `
		#version 120
		#ifdef GL_ES
			precision mediump float;
		#endif
		varying vec4 color;
		void main()
		{
		    gl_FragColor = color;
		}` + "\x00"
)

// Viewer defines a 3D geometry viewer
type Viewer struct {
	Views      map[int]View
	GuiViews   map[int]View
	ClearColor graphics.Color

	camera                                    *geom.FixedCamera
	viewID                                    int
	program                                   uint32
	mvpLocation, vposLocation, vcolLocation   uint32
	distance                                  float32
	objectTransform                           geom.Matrix4f
	center, sphereClickPoint, sphereDragPoint geom.Vec3f
	mouseClickPoint                           geom.Vec2
	dragRotation, objectRotation              *geom.Quaternionf
	dragging, panning                         bool
	sphere                                    geom.Sphere
}

// View defines a generic viewable 3D object
type View interface {
	AABB() *geom.AABB
	Selection() interface{} // Returns an interface which should be casted to the appropriate slice of object type
	ClearSelection()
	SetVisible(visible bool)

	OnRender()
	OnRelease()
	OnUpdate()
	OnRectSelect(vx, vy, vw, vh int)
}

// NewViewer creates a new Viewer
func NewViewer() *Viewer {
	return &Viewer{Views: make(map[int]View), GuiViews: make(map[int]View)}
}

// Init initializes the Viewer
func (v *Viewer) Init(width, height int) error {

	var err error

	// Initialize Glow
	if err = gl.Init(); err != nil {
		panic(err)
	}

	gl.Enable(gl.VERTEX_PROGRAM_POINT_SIZE)

	// Configure the vertex and fragment shaders
	v.program, err = newProgram(vShaderSource, fShaderSource)
	if err != nil {
		panic(err)
	}

	// Setup shader
	gl.UseProgram(v.program)

	v.mvpLocation = uint32(gl.GetUniformLocation(v.program, gl.Str("MVP\x00")))
	v.vposLocation = uint32(gl.GetAttribLocation(v.program, gl.Str("vPos\x00")))
	v.vcolLocation = uint32(gl.GetAttribLocation(v.program, gl.Str("vCol\x00")))

	v.dragRotation = geom.NewQuaternionIdentity()
	v.objectRotation = geom.NewQuaternionIdentity()

	v.camera = geom.NewFixedCamera([4]int{0, 0, width, height}, 0.1, 100, 45, geom.Vec3f{X: 0, Y: 0, Z: 10}, geom.Vec3f{X: 0, Y: 0, Z: 0})
	v.Resize(width, height)

	v.sphere = geom.Sphere{Center: geom.Vec3d{X: float64(width) / 2.0, Y: float64(height) / 2.0, Z: 0}, Radius: float64(width) / 2.0}

	v.ClearColor.R = 0.25
	v.ClearColor.G = 0.25
	v.ClearColor.B = 0.25
	v.ClearColor.A = 1.0

	return nil
}

// Resize resizes the Viewer
func (v *Viewer) Resize(width, height int) {
	v.camera.Viewport[2] = width
	v.camera.Viewport[3] = height

	// Setup GL viewport
	gl.Viewport(0, 0, int32(width), int32(height))
}

// Render renders one frame
func (v *Viewer) Render() {

	gl.ClearColor(v.ClearColor.R, v.ClearColor.G, v.ClearColor.B, v.ClearColor.A)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	// Object rotation
	totalRotation := v.objectRotation
	if v.dragging {
		totalRotation = geom.QuaternionfMul(totalRotation, v.dragRotation)
	}

	cloudTransform := totalRotation.ToMatrix()
	v.camera.Update(cloudTransform)

	for _, view := range v.Views {
		view.OnRender()
	}

	gl.UniformMatrix4fv(int32(v.mvpLocation), 1, false, &v.camera.Mvp[0])

	for _, view := range v.GuiViews {
		view.OnRender()
	}

}

// AddObject adds an object to the viewer
func (v *Viewer) AddObject(object interface{}, isGui bool) {

	var view View

	switch object.(type) {
	case *pct.PointCloud:
		view = NewPointCloudView(object.(*pct.PointCloud), v)
	case *mt.Mesh:
		view = NewMeshView(object.(*mt.Mesh), v)
	case *Text:
		view = NewTextView(object.(*Text), v)
	case *Frustum:
		view = NewFrustumView(object.(*Frustum), v)
	case *Circle:
		view = NewCircleView(object.(*Circle), v)
	}

	if isGui {
		v.GuiViews[v.viewID] = view
	} else {
		v.Views[v.viewID] = view
	}

	v.viewID++

	aabb := geom.NewAABB()

	for _, view := range v.Views {
		aabb = geom.AABBUnion(aabb, view.AABB())
	}

	v.camera.Center = geom.Vec3d2Vec3f(aabb.Box.Center())
	v.distance = float32(aabb.Box.Extent()) * 3.0
	v.camera.Eye = v.camera.Center.Add(geom.Vec3f{X: 0, Y: 0, Z: v.distance})
	v.camera.ZNear = 0.1
	v.camera.ZFar = float32(aabb.Box.Extent()) * 8.0

	v.Render()
}

// RemoveObject removes an object from the viewer
func (v *Viewer) RemoveObject(viewID int) {
	_, ok := v.Views[viewID]

	if ok {
		delete(v.Views, viewID)
	}
}

// RemoveGuiObject removes an object from the viewer
func (v *Viewer) RemoveGuiObject(viewID int) {
	_, ok := v.GuiViews[viewID]

	if ok {
		delete(v.GuiViews, viewID)
	}
}

// Dispose event
func (v *Viewer) Dispose() {
	for i := range v.Views {
		v.Views[i] = nil
	}
}

// OnMousePressed event
func (v *Viewer) OnMousePressed(button int, x, y float32) {

	switch button {
	case 0:
		v.dragging = true
		v.sphereClickPoint = geom.Vec3d2Vec3f(v.sphere.UnProject(float64(x), float64(y)))
	case 1:
		v.panning = true
		v.mouseClickPoint = geom.Vec2{X: float64(x), Y: float64(y)}
	}
}

// OnMouseMoved event
func (v *Viewer) OnMouseMoved(x, y float32) {
	if v.dragging {
		v.sphereDragPoint = geom.Vec3d2Vec3f(v.sphere.UnProject(float64(x), float64(y)))
		v.dragRotation = geom.NewQuaternionVectors(v.sphereClickPoint, v.sphereDragPoint)
	}
	if v.panning {
		v.camera.Shift.X = (float32(v.mouseClickPoint.X) - x) * v.distance * 0.0012
		v.camera.Shift.Y = (y - float32(v.mouseClickPoint.Y)) * v.distance * 0.0012
	}
}

// OnMouseReleased event
func (v *Viewer) OnMouseReleased(button int, x, y float32) {
	switch button {
	case 0:
		v.dragging = false
		v.objectRotation = geom.QuaternionfMul(v.objectRotation, v.dragRotation)
		v.dragRotation.SetIdentity()
	case 1:
		v.panning = false
		v.camera.Eye = v.camera.Eye.Add(v.camera.Shift)
		v.camera.Shift = geom.Vec3f{X: 0, Y: 0, Z: 0}
	}
}

// OnFOVChanged updates the viewer field of view
func (v *Viewer) OnFOVChanged(xoff, yoff float64) {
	if yoff > 0 {
		v.camera.Fov *= 0.9
		v.distance *= 0.9
	} else {
		if v.camera.Fov < 150 {
			v.camera.Fov *= 1.1
			v.distance *= 1.1
		}
	}

	v.Render()
}

func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}
