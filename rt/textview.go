package rt

import (
	"os"

	"golang.org/x/image/math/fixed"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/commontools/graphics"
	"github.com/4ydx/gltext"
	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/mathgl/mgl32"
)

// Text is a text to be displayed
type Text struct {
	Text, TtfPath string
	Position      geom.Vec3d
	Size          int
	Color         graphics.Color
}

// TextView a text view
type TextView struct {
	Text *Text

	glFont *gltext.Font
	glText *gltext.Text
	viewer *Viewer
	dirty  bool
}

// NewTextView creates a new text view
func NewTextView(text *Text, viewer *Viewer) *TextView {
	return &TextView{Text: text, dirty: true, viewer: viewer}
}

// AABB override
func (view *TextView) AABB() *geom.AABB {
	aabb := geom.NewAABB()
	aabb.ExtendTo(view.Text.Position)
	return aabb
}

// Selection override
func (view *TextView) Selection() interface{} {
	return nil
}

// ClearSelection override
func (view *TextView) ClearSelection() {

}

// SetVisible override
func (view *TextView) SetVisible(visible bool) {

}

// OnRender override
func (view *TextView) OnRender() {

	if view.dirty {
		view.OnUpdate()
		view.dirty = false
	}

	gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	gl.Disable(gl.DEPTH_TEST)
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	position := view.viewer.camera.Project(geom.Vec3d2Vec3f(view.Text.Position))
	viewport := view.viewer.camera.Viewport
	view.glText.SetPosition(mgl32.Vec2{
		float32(position.X - float32(viewport[2])/2.0),
		float32(position.Y - float32(viewport[3])/2.0)})
	view.glText.Draw()
	gl.Disable(gl.BLEND)
}

// OnRelease override
func (view *TextView) OnRelease() {

}

// OnUpdate override
func (view *TextView) OnUpdate() {
	fd, err := os.Open(view.Text.TtfPath)
	if err != nil {
		panic(err)
	}
	defer fd.Close()

	runeRanges := make(gltext.RuneRanges, 0)
	runeRange := gltext.RuneRange{Low: 2, High: 127}
	runeRanges = append(runeRanges, runeRange)
	view.glFont, err = gltext.NewTruetype(fd, fixed.Int26_6(view.Text.Size), runeRanges, fixed.Int26_6(128))

	if err != nil {
		panic(err)
	}

	view.glFont.ResizeWindow(float32(view.viewer.camera.Viewport[2]), float32(view.viewer.camera.Viewport[3]))

	view.glText = gltext.NewText(view.glFont, float32(1.0), float32(1.1))
	view.glText.SetString(view.Text.Text)
	view.glText.SetColor(mgl32.Vec3{view.Text.Color.R, view.Text.Color.G, view.Text.Color.B})
}

// OnRectSelect override
func (view *TextView) OnRectSelect(vx, vy, vw, vh int) {

}
