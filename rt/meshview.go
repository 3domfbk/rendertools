package rt

import (
	"unsafe"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/meshtools/mt"
	"github.com/go-gl/gl/v4.1-core/gl"
)

// MeshView a point cloud view
type MeshView struct {
	Mesh *mt.Mesh

	viewer         *Viewer
	dirty          bool
	posVbo, colVbo uint32
	indices        []uint32
}

// NewMeshView creates a new point cloud view
func NewMeshView(mesh *mt.Mesh, viewer *Viewer) *MeshView {
	return &MeshView{Mesh: mesh, dirty: true, viewer: viewer}
}

// AABB returns the point cloud BBox
func (v *MeshView) AABB() *geom.AABB {
	return v.Mesh.BBox
}

// Selection override
func (v *MeshView) Selection() interface{} {
	return nil
}

// ClearSelection override
func (v *MeshView) ClearSelection() {

}

// SetVisible override
func (v *MeshView) SetVisible(visible bool) {

}

// OnRender override
func (v *MeshView) OnRender() {

	if v.dirty {
		v.OnUpdate()
		v.dirty = false
	}

	gl.UseProgram(v.viewer.program)

	// Positions
	gl.BindBuffer(gl.ARRAY_BUFFER, v.posVbo)
	gl.EnableVertexAttribArray(v.viewer.vposLocation)
	gl.VertexAttribPointer(v.viewer.vposLocation, 3, gl.DOUBLE, false,
		int32(unsafe.Sizeof(v.Mesh.Positions()[0])), gl.PtrOffset(0))

	// Colors
	gl.BindBuffer(gl.ARRAY_BUFFER, v.colVbo)
	gl.EnableVertexAttribArray(v.viewer.vcolLocation)
	gl.VertexAttribPointer(v.viewer.vcolLocation, 3, gl.UNSIGNED_BYTE, true, 3, gl.PtrOffset(0))

	gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, 0)
	gl.DrawElements(gl.TRIANGLES, int32(len(v.indices)), gl.UNSIGNED_INT, gl.Ptr(v.indices))
}

// OnRelease override
func (v *MeshView) OnRelease() {

}

// OnUpdate override
func (v *MeshView) OnUpdate() {
	// Positions
	gl.GenBuffers(1, &v.posVbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, v.posVbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(v.Mesh.Positions())*int(unsafe.Sizeof(v.Mesh.Positions()[0])), gl.Ptr(v.Mesh.Positions()), gl.STATIC_DRAW)

	// Colors
	colors := make([]uint8, len(v.Mesh.Positions())*3)
	if v.Mesh.R() != nil {
		for i := range v.Mesh.Positions() {
			colors[i*3+0] = v.Mesh.R()[i]
			colors[i*3+1] = v.Mesh.G()[i]
			colors[i*3+2] = v.Mesh.B()[i]
		}
	} else {
		for i := range v.Mesh.Positions() {
			colors[i*3+0] = 0xFF
			colors[i*3+1] = 0xFF
			colors[i*3+2] = 0xFF
		}
	}
	gl.GenBuffers(1, &v.colVbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, v.colVbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(colors), gl.Ptr(colors), gl.STATIC_DRAW)

	// Indices
	v.indices = make([]uint32, 0, len(v.Mesh.Faces)*3)

	for _, face := range v.Mesh.Faces {
		v.indices = append(v.indices, face.HalfEdge.Vertex)
		v.indices = append(v.indices, face.HalfEdge.Next.Vertex)
		v.indices = append(v.indices, face.HalfEdge.Next.Next.Vertex)
	}
}

// OnRectSelect override
func (v *MeshView) OnRectSelect(vx, vy, vw, vh int) {

}
