package rt

import (
	"unsafe"

	"bitbucket.org/3domfbk/commontools/geom"
	"bitbucket.org/3domfbk/pctools/pct"
	"github.com/go-gl/gl/v4.1-core/gl"
)

// PointCloudView a point cloud view
type PointCloudView struct {
	PointCloud *pct.PointCloud

	viewer                 *Viewer
	dirty                  bool
	posVbo, norVbo, colVbo uint32
}

// NewPointCloudView creates a new point cloud view
func NewPointCloudView(pc *pct.PointCloud, viewer *Viewer) *PointCloudView {
	return &PointCloudView{PointCloud: pc, dirty: true, viewer: viewer, colVbo: 0, norVbo: 0}
}

// AABB returns the point cloud BBox
func (v *PointCloudView) AABB() *geom.AABB {
	return v.PointCloud.BBox
}

// Selection override
func (v *PointCloudView) Selection() interface{} {
	return nil
}

// ClearSelection override
func (v *PointCloudView) ClearSelection() {

}

// SetVisible override
func (v *PointCloudView) SetVisible(visible bool) {

}

// OnRender override
func (v *PointCloudView) OnRender() {

	if v.dirty {
		v.OnUpdate()
		v.dirty = false
	}

	// Positions
	gl.BindBuffer(gl.ARRAY_BUFFER, v.posVbo)
	gl.EnableVertexAttribArray(v.viewer.vposLocation)
	gl.VertexAttribPointer(v.viewer.vposLocation, 3, gl.DOUBLE, false,
		3*8, gl.PtrOffset(0))

	// VColors
	gl.BindBuffer(gl.ARRAY_BUFFER, v.colVbo)
	gl.EnableVertexAttribArray(v.viewer.vcolLocation)
	gl.VertexAttribPointer(v.viewer.vcolLocation, 3, gl.UNSIGNED_BYTE, true,
		3, gl.PtrOffset(0))

	// Render
	gl.Enable(gl.DEPTH_TEST)
	gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	gl.DrawArrays(gl.POINTS, 0, int32(len(v.PointCloud.Positions())))
}

// OnRelease override
func (v *PointCloudView) OnRelease() {

}

// OnUpdate override
func (v *PointCloudView) OnUpdate() {
	positions := v.PointCloud.Positions()
	normals := v.PointCloud.Normals()
	r, g, b := v.PointCloud.R(), v.PointCloud.G(), v.PointCloud.B()

	gl.GenBuffers(1, &v.posVbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, v.posVbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(positions)*int(unsafe.Sizeof(positions[0])), gl.Ptr(positions), gl.STATIC_DRAW)

	if normals != nil {
		gl.GenBuffers(1, &v.norVbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, v.norVbo)
		gl.BufferData(gl.ARRAY_BUFFER, len(normals)*int(unsafe.Sizeof(normals[0])), gl.Ptr(normals), gl.STATIC_DRAW)
	}

	if r != nil {
		// Create interleaved array for vcolors, shame shame
		colors := make([]uint8, len(r)*3)
		for i := 0; i < len(r); i++ {
			colors[i*3+0] = r[i]
			colors[i*3+1] = g[i]
			colors[i*3+2] = b[i]
		}

		gl.GenBuffers(1, &v.colVbo)
		gl.BindBuffer(gl.ARRAY_BUFFER, v.colVbo)
		gl.BufferData(gl.ARRAY_BUFFER, len(colors), gl.Ptr(colors), gl.STATIC_DRAW)
	}
}

// OnRectSelect override
func (v *PointCloudView) OnRectSelect(vx, vy, vw, vh int) {

}
